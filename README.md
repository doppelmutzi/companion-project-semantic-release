# Companion project for blog article

This is a companion project for [my article](https://blog.logrocket.com/author/sebastianweber/) about semantic release.

## Using locally

```sh
$ npm run calculator
```

## Using published package

With provided npm auth token and config, you can execute the version with `@latest` dist-tag:

```sh
$ npx @doppelmutzi/semantic-release-git-workflow@latest doppelmutzi-calculator
```

Or execute the version with the `@next` dist-tag.
```sh
$ npx @doppelmutzi/semantic-release-git-workflow@next doppelmutzi-calculator
```

### How to use

You can use an optional CLI option `-h` or `--help` for further information.
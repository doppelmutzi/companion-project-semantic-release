## [2.1.0](https://gitlab.com/doppelmutzi/companion-project-semantic-release/compare/v2.0.0...v2.1.0) (2023-12-5)


### Features

* add -h / --help CLI option ([7c96028](https://gitlab.com/doppelmutzi/companion-project-semantic-release/commit/7c960286a471c4e826e6259260f75015b63b8ff0))

## [2.0.0](https://gitlab.com/doppelmutzi/companion-project-semantic-release/compare/v1.1.1...v2.0.0) (2023-12-5)


### ⚠ BREAKING CHANGES

* the CLI tool only works with Node 18.19.0 or greater

### Miscellaneous Chores

* upgrade engines field ([0aeb83e](https://gitlab.com/doppelmutzi/companion-project-semantic-release/commit/0aeb83e8559db4d6558d2f3236e15c459d591116))

## [1.1.1](https://gitlab.com/doppelmutzi/companion-project-semantic-release/compare/v1.1.0...v1.1.1) (2023-12-5)


### Bug Fixes

* command accepts whitespaces and unlimited numbers can be added ([791e44a](https://gitlab.com/doppelmutzi/companion-project-semantic-release/commit/791e44abb6cd74cde8a58bab0a0aa52e236ee2cd))


### Miscellaneous Chores

* **release:** 1.0.2 [skip ci] ([8ee8fb4](https://gitlab.com/doppelmutzi/companion-project-semantic-release/commit/8ee8fb4e489e9e73de757ab0a0dfcd76252d6694))

## [1.1.0](https://gitlab.com/doppelmutzi/companion-project-semantic-release/compare/v1.0.1...v1.1.0) (2023-12-5)


### Features

* allow subtraction ([20f8d03](https://gitlab.com/doppelmutzi/companion-project-semantic-release/commit/20f8d032e59679ef5a508f4779558a19810e39af))

## [1.0.2](https://gitlab.com/doppelmutzi/companion-project-semantic-release/compare/v1.0.1...v1.0.2) (2023-12-5)


### Bug Fixes

* command accepts whitespaces and unlimited numbers can be added ([791e44a](https://gitlab.com/doppelmutzi/companion-project-semantic-release/commit/791e44abb6cd74cde8a58bab0a0aa52e236ee2cd))

## [1.0.1](https://gitlab.com/doppelmutzi/companion-project-semantic-release/compare/v1.0.0...v1.0.1) (2023-12-5)


### Miscellaneous Chores

* **README:** how to use the CLI tool ([7433afb](https://gitlab.com/doppelmutzi/companion-project-semantic-release/commit/7433afbec8cb44b676bf91121eaa77d1fcdf5f64))

## 1.0.0 (2023-12-3)


### Features

* initial CLI tool with addition functionality ([2a753be](https://gitlab.com/doppelmutzi/companion-project-semantic-release/commit/2a753be61806ade4bf9d9a2e69582a9799f94e6c))


### Miscellaneous Chores

* enable semantic release by removing --dry-run option ([74c4776](https://gitlab.com/doppelmutzi/companion-project-semantic-release/commit/74c477655ed3708472b5bbaa5d7ddec6a335e850))

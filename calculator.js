#!/usr/bin/env node

const readline = require('readline');
const argv = process.argv.slice(2);

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

if (argv.includes('-h') || argv.includes('--help')) {
  console.log('Usage: calculator.js [OPTIONS]');
  console.log('Options:');
  console.log('  -h, --help   Display this help message.');
  rl.close();
} else {
  rl.question('Enter the operation in the format "<Number>+<Number>-<Number>+...": ', (input) => {
    const operations = input.split(/([+-])/).map((item) => item.trim());

    let result = 0;
    let currentOperator = '+';

    operations.forEach((operation) => {
      if (operation === '+' || operation === '-') {
        currentOperator = operation;
      } else {
        const num = parseInt(operation);
        if (!isNaN(num)) {
          if (currentOperator === '+') {
            result += num;
          } else if (currentOperator === '-') {
            result -= num;
          }
        } else {
          console.log(`Invalid format. Please enter operations in the format "<Number>+<Number>-<Number>+...".`);
          rl.close();
          return;
        }
      }
    });

    console.log(`Result of Operations: ${result}`);
    rl.close();
  });
}

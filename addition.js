#!/usr/bin/env node

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Enter numbers to add in the format "<Number>+<Number>+...": ', (input) => {
  const numbers = input.split('+').map((num) => parseInt(num.trim()));

  if (numbers.every((num) => !isNaN(num))) {
    const result = numbers.reduce((acc, num) => acc + num, 0);
    console.log(`Result of Addition: ${result}`);
  } else {
    console.log('Invalid format. Please enter numbers in the format "<Number>+<Number>+...".');
  }

  rl.close();
});
